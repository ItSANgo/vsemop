/**
 * @file vsemop_usage.c
 * @author Mitsutoshi Nakano (ItSANgo@gmail.com)
 * @brief System V semaphore operation command - usage.
 * @version 0.1.0.0
 * @date 2019-12-22
 * 
 * @copyright Copyright (c) 2019  Mitsutoshi Nakano
 * 
 */

#include "vsemop.h"

/**
 * @brief Print usage, and exit.
 * 
 * @param status exit status.
 * @param fp 
 * @param format 
 * @param ... 
 */
void
vsemop_usage(int status, FILE *fp)
{
    fprintf(fp, "Usage: vsemop key file.\n");
    exit(status); /*NOTREACHED*/
}
