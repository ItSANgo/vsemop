/**
 * @file vsemop_main.c
 * @author Mitsutoshi Nakano (ItSANgo@gmail.com)
 * @brief System V semaphore operation command - main.
 * @version 0.1.0.0
 * @date 2019-12-22
 * 
 * @copyright Copyright (c) 2019  Mitsutoshi Nakano
 * 
 */


#include "vsemop.h"

main(int argc, char *argv[])
{
    if (argc != 3) {
        vsemop_usage(EX_USAGE, stderr); /*NOTREACHED*/
    }
    int id = atoi(argv[1]);

    FILE *fp;
    if ((fp = fopen(argv[2], "r") ) == NULL) {
        err(EX_NOINPUT, "%s", argv[2]); /*NOTREACHED*/
    }
    char buf[BUFSIZ];
    while (fgets(buf, BUFSIZ, fp)) {
        char *next = buf;
        long number = strtol(buf, &next, 10);
        long operation = strtol(next, &next, 10);
        struct sembuf semaphore_operations[1] = {{number, operation}};
        if (semop (id, semaphore_operations, 1u)) {
            err(EX_DATAERR, "semop(%d, {%ld, %ld})", id, number, operation);
        }
    }
}
