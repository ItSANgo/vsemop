#ifndef _VESMOP_H
#define _VSEMOP_H 1

/**
 * @file vsemop.h
 * @author Mitsutoshi Nakano (ItSANgo@gmail.com)
 * @brief System V semaphore operation command.
 * @version 0.1.0.0
 * @date 2019-12-22
 * 
 * @copyright Copyright (c) 2019  Mitsutoshi Nakano
 * 
 */

#include <sysexits.h>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <unistd.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

extern void vsemop_usage(int status, FILE *fp);

#endif /* VSEMOP_H.  */
